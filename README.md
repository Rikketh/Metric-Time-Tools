# Metric Time Tools
Because we divide by 10

# What is this
Metric Time Tools repo contains a set of scripts written in python 3 designed to help with metric time conversion.

-----------------------------------------------------------------------------------------------------

# Available tools
### Clock ###
The clock is available via the `clock.py`. It simply prints the time in different formats every single SI second:

* UTC (HH:MM:SS)
* Standard time (Anglo-Babylonian Time, ABT) with your positive/negative UTC offset
* Local Metric Time
* Universal Metric Time

### ABT-to-MT converter
Simply allows you to convert `HH:MM(:SS)` time format to decimal (metric) time format (either approximate or exact, depends on units). Accepts three most used measuring units:

* Dez (1/10 of a day)
* Cent (1/100 of a day)
* Mil (1/1000 of a day)

Also allows you to convert time to either Local MT or Universal MT. You can omit seconds in order to create an approximate value when converting:

* `20:55` is acceptable
* But `20:55:23` will also do, the difference is the precision

Keep in mind that 12-hour format is illegal.

### MT-to-ABT converter
Pretty much the same as above, except for does exactly opposite thing. And is not that flexible. And is not that cool.  
It only accepts Cent as the input time (though you can convert Dez to Cent by multiplying Dez time by 10).

Now, in case you do not have precise Cent after converting Dez to Cent (which is most likely due to the nature of Dez), you can either use trailing zeros or just leave it as is.

Keep in mind that using approximate input will give you less precise ABT so it might end up being off by two or three minutes.  
In case your input time is less than 10, you **have to** include leading zero: `01.750`.

Also don’t forget that backwards conversion on precise Cent is usually floored, so the exact time might me off by around 1.7 SI seconds. Not that it is a big deal, but just keep that in mind.

### Command-line converter
Is pretty much both described above, except for it is meant to be used for quick one-line current time to metric time conversion. Use `python3 cmdconv.py -h` to list all available arguments and options.  
A heads up: when converting to ABT, you may drop units. MT-to-ABT requires cents to be passed in.

-----------------------------------------------------------------------------------------------------

## Metric Time overview
### What is MT?
Metric time is a format of time that bases on decimal values. It is natural for humans to use base-10 numbers (despite the fact that it is not really _that_ convenient to perform math operations on these numbers).

Unlike Anglo-Babylonian time (the 24/60/60 format), Metric time is, as stated above, base-10-based numbers, which is much more simple to use in general. It divides a standard day (which consists of 86400 SI seconds) into 10 (dez), 100 (cent) or 1000 (mil) periods, so you can actually have a proper 100% progress bar, yay!  
Using floating point values also points to a specific time between two periods to specify more precise time.

### Why would you use MT?
I don’t know, tbh. It looks cool. It sounds cool (on paper). It does not relate to Britain. It does not introduce negative timezones (yay unsigneds?) and so on.

Definetely, there’s one thing I find really useful: low-precision cents (HH:MM conversion) remains two-to-three digits long (not counting the dot) and yet conveys precise enough values to relate to during the day. Same works for the precise cents: they are two characters shorter than ABT with its seconds, yet remain as precise as ABT (`xx.xxx` / `xx:xx:xx`).

### What’s the deal with timezones and global time?
UTC/GMT nowadays has 24 time zones that are spread across the globe in a slightly retarded way: we have 12 positive and 12 *negative* timezones. This is all kind of historically related to UK being a huge empire that once owned an enormous amount of land on Earth. But these times are long gone. Not the habits, though.

The International Date Line is for the rescue, though! Historically neutral, it is located just at the tight spot where the negative and positive timezones meet, basically getting rid of negative zones.

### Measure units
There are three units, they all relate to one 24-hour day (which consists of 86400 SI seconds):

* Dez (deciday), the `1/10` of a day
* Cent (centiday), the `1/100` of a day
* Mil (milliday), the `1/1000` of a day

The most useful at this point is cent: it both divides a day into 100 equal units (both representing percentage and hours), also giving the ability to specify both approximate and exact time. Dez is the second useful unit that divides the day into 10 units. Good for time-consuming activities and such, when you don’t need whole 100.

### What’s the deal with the SI second?
It ain’t good lad. Currently SI seconds are still used as a unit that we rely on. And it is not that nice number that you see all across this readme. 86400 seconds in a day fuck things up just a bit. But there are some solutions:

* Bring in a new unit that would represent a second and would last `1/100000` of a day (and name it `chron`, maybe?)
* Forget it and use relation of `1 Mil` = `8.64 SIs` when converting

But daily life would not be affected by the presence of this issue while we still have **SI seconds** and **MT Millidays**. At least not that drastically.

Inspired by [this set of articles](http://zapatopi.net/metrictime/).