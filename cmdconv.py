import time
import re
import math
import argparse

units = {"d":1, "c":2, "m":3}

parser = argparse.ArgumentParser(prog="Metric Time Commandline Tools", description="Converts time back and forth using arguments")
parser.add_argument("mode", help="Target converesion mode", choices=('abt', 'mt'), default="mt", type=str, nargs="?")
parser.add_argument("input", help="Input time in either ABT or MT form: HH:MM(:SS) or CC.C(CC)", nargs="?", default=time.strftime("%H:%M:%S"))
parser.add_argument("unit", help="Target converesion units", choices=units, default="c", type=str, nargs="?")
parser.add_argument("-u", "--umt", help="Convert time to UMT instead of LMT", default=False, action="store_true")
args = parser.parse_args()

unit = units.get(args.unit)

try:

	if args.mode == 'abt':
		if not bool(re.match("(\d{2}\.\d{1,3})", str(args.input), re.IGNORECASE)):
			raise RuntimeError('Can not use the specified input time. Please provice cents.')

		lABT = [None, None, None] #h, m, s
		args.input = float(args.input)

		lABT[0] = math.floor(args.input/100*24)
		lABT[1] = math.floor(((args.input/100*24) - lABT[0])*60)
		lABT[2] = math.floor(((((args.input/100*24) - lABT[0])*60) - lABT[1])*60) #ouchie

		if args.umt:
			lABT[0] = lABT[0] - 12; #switch back to UTC0
			if lABT[0] < 0: lABT[0] += 24 #and in case we’ve overswitched somehow, fix it by rolling back a day

			lABT_localtz = lABT[:]; lABT_localtz[0] = round(lABT_localtz[0] - time.timezone/60/60) #this time we’re just calculating the local time based on the UTC0
			if lABT_localtz[0] < 0: lABT_localtz[0] += 24
			elif lABT_localtz[0] > 24: lABT_localtz[0] -=24

			lABT = list(map(str, lABT))
			lABT_localtz = list(map(str, lABT_localtz))


			results = {
				"utc0": time.strftime("%H:%M:%S", time.strptime(':'.join(lABT), "%H:%M:%S")),
				"offset": -time.timezone/60/60,
				"utctime": time.strftime("%H:%M:%S", time.strptime(':'.join(lABT_localtz), "%H:%M:%S"))
			}
		
			print('UTC0 time should be: {0}\nUTC{1} time should be: {2}'.format(results["utc0"], results["offset"], results["utctime"]))
		else:
			lABT = list(map(str, lABT))
			print('Local time should be: {0}'.format(time.strftime("%H:%M:%S", time.strptime(':'.join(lABT), "%H:%M:%S"))))
	elif args.mode == 'mt':
		l_Time = list(map(int, args.input.split(":")))
		if args.umt:
			i_IDLTime = l_Time[0] + (time.timezone/60/60 - 12)
			if(i_IDLTime < 0): i_IDLTime += 24

			if len(l_Time) == 2:
				f_MTTime = ((l_Time[1]/1440) + (i_IDLTime/24)) * 10**unit
			else:
				f_MTTime = ((l_Time[2]/86400) + (l_Time[1]/1440) + (i_IDLTime/24)) * 10**unit

			print('UMT would be {0:.3f}'.format(f_MTTime))
		else:
			if len(l_Time) == 2:
				f_MTTime = ((l_Time[1]/1440) + (l_Time[0]/24)) * 10**unit
			else:
				f_MTTime = ((l_Time[2]/86400) + (l_Time[1]/1440) + (l_Time[0]/24)) * 10**unit

			print('LMT would be {0:.3f}'.format(f_MTTime))

except RuntimeError as ex:
	print("[ERROR] Converter terminated: {0}.".format(ex.args))
except Exception as ex:
	print("\n[CRASH] An error occurred: {0}.".format(ex.args))