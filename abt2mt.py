import time #for the sake of timezones
import re #gotta simply check if user inputs HH:MM:SS or HH:MM

g_converterSettings = {}

def interactive_setup():
	g_converterSettings['units'] = int(input("What MT measuring units do you want to convert the time to?\n\t1 = dez\n\t2 = cent\n\t3 = mil\n> "))
	if g_converterSettings['units'] < 1 or g_converterSettings['units'] > 3:
		raise RuntimeError('Unknown unit index specified');

	g_converterSettings['timezone'] = input("Use your local timezone = L\nUse Universal Metric Time = U\n> ").lower()
	if type(g_converterSettings['timezone']) == "str" or g_converterSettings['timezone'] not in ('l', 'u'):
		raise RuntimeError('Incorrect timezone identifier specified');

	g_converterSettings['sample'] = str(input("Provide the time that needs to be converted in the format of HH:MM(:SS)\n> "))
	if not bool(re.match("(\d{2}:\d{2})(:\d{2})?", str(g_converterSettings['sample']), re.IGNORECASE)):
		raise RuntimeError('The time you have provided does not match the required format');

def convert():
	l_Time = list(map(int, g_converterSettings['sample'].split(":")))
	if g_converterSettings['timezone'] == 'l':
		if len(l_Time) == 2:
			f_MTTime = ((l_Time[1]/1440) + (l_Time[0]/24)) * 10**g_converterSettings['units']
		else:
			f_MTTime = ((l_Time[2]/86400) + (l_Time[1]/1440) + (l_Time[0]/24)) * 10**g_converterSettings['units']

		return 'LMT would be {0:.3f}'.format(f_MTTime)
	else:
		i_IDLTime = l_Time[0] + (time.timezone/60/60 - 12)
		if(i_IDLTime < 0): i_IDLTime += 24

		if len(l_Time) == 2:
			f_MTTime = ((l_Time[1]/1440) + (i_IDLTime/24)) * 10**g_converterSettings['units']
		else:
			f_MTTime = ((l_Time[2]/86400) + (l_Time[1]/1440) + (i_IDLTime/24)) * 10**g_converterSettings['units']

		return 'UMT would be {0:.3f}'.format(f_MTTime)
try:
	interactive_setup()
	print(convert())
except RuntimeError as ex:
	print("[ERROR] Converter terminated: {0}.".format(ex.args))
except Exception as ex:
	print("[CRASH] An error occurred: {0}.".format(ex.args))