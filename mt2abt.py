import time #for the sake of timezones
import re #gotta simply check if user inputs CC.DDD or CC.D
import math

g_dConvSettings = {}
print("""========================= WARNING =========================
| Time conversion results might differ by a second or two |
|            Conversion results are approximate           |
===========================================================
""")

def interactive_setup():
	g_dConvSettings['timezone'] = input("The time I have is Local = L\nThe time I have is Universal = U\n> ").lower()
	if type(g_dConvSettings['timezone']) == "str" or g_dConvSettings['timezone'] not in ('l', 'u'):
		raise RuntimeError('Incorrect timezone identifier specified');

	g_dConvSettings['mt'] = float(input("Please provide the time in cents, keeping leading and trailing zeros\n> "))
	if not bool(re.match("(\d{2}\.\d{1,3})", str(g_dConvSettings['mt']), re.IGNORECASE)):
		raise RuntimeError('The time you have provided does not match standard Cent pattern');

def convert():
	lABT = [None, None, None] #h, m, s

	lABT[0] = math.floor(g_dConvSettings['mt']/100*24)
	lABT[1] = math.floor(((g_dConvSettings['mt']/100*24) - lABT[0])*60)
	lABT[2] = math.floor(((((g_dConvSettings['mt']/100*24) - lABT[0])*60) - lABT[1])*60)

	if g_dConvSettings['timezone'] == 'l':
		lABT = list(map(str, lABT))
		return 'Local time should be: {0}'.format(time.strftime("%H:%M:%S", time.strptime(':'.join(lABT), "%H:%M:%S")))
	else:
		lABT[0] = lABT[0] - 12; #switch back to UTC0
		if lABT[0] < 0: lABT[0] += 24 #and in case we’ve overswitched somehow, fix it by rolling back a day

		lABT_localtz = lABT[:]; lABT_localtz[0] = round(lABT_localtz[0] - time.timezone/60/60) #this time we’re just calculating the local time based on the UTC0
		if lABT_localtz[0] < 0: lABT_localtz[0] += 24
		elif lABT_localtz[0] > 24: lABT_localtz[0] -=24

		lABT = list(map(str, lABT))
		lABT_localtz = list(map(str, lABT_localtz))

		results = {
			"utc0": time.strftime("%H:%M:%S", time.strptime(':'.join(lABT), "%H:%M:%S")),
			"offset": -time.timezone/60/60,
			"utctime": time.strftime("%H:%M:%S", time.strptime(':'.join(lABT_localtz), "%H:%M:%S"))
		}

		return 'UTC0 time should be: {0}\nUTC{1} time should be: {2}'.format(results["utc0"], results["offset"], results["utctime"])
try:
	interactive_setup()
	print(convert())
except RuntimeError as ex:
	print("[ERROR] Converter terminated: {0}.".format(ex.args))
except Exception as ex:
	print("\n[CRASH] An error occurred: {0}.".format(ex.args))